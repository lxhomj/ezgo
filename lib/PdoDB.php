<?php
require_once 'DBConfig.php';
require_once 'Statistics.php';
/**
 *
 *
 * Pdo 数据访问层
 * 单例实现
 * 
 * @author maxwell
 *        
 */
class PdoDB
{
	private static $_instance = NULL;
	private static $_DBH = null;
	public static $errCode = 0;
		
	/**
	 * 错误信息
	 *
	 * @var string
	 */
	public static $errMsg = '';
	
	/**
	 * 清除错误标识，在每个函数调用前调用
	 */
	private function clearERR()
	{
		$this->errCode = 0;
		$this->errMsg = '';
	}
	/**
	 * 私有构造函数，
	 */
	private function __construct()
	{
	}
	/**
	 * 私有克隆函数
	 */
	private function __clone()
	{
	}
	/**
	 * 获取数据库连接对象
	 * 
	 * @return PdoDB
	 */
	static public function getInstance()
	{
		if (is_null ( self::$_instance ) || ! isset ( self::$_instance ))
		{
			self::$_instance = new self ();
			try
			{
				$DBAddress = DBConfig::$DBConfigArray ['default'] ['DBAddress'];
				$user = DBConfig::$DBConfigArray ['default'] ['user'];
				$password = DBConfig::$DBConfigArray ['default'] ['password'];
				self::$_DBH = new PDO ( $DBAddress, $user, $password, array (
						PDO::ATTR_PERSISTENT => true 
				) );
				self::$_DBH->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
			} catch ( PDOException $e )
			{
				self::$errCode = 1;
				self::$errMsg = $e->getmessage ();
				die ();
			}
		}
		return self::$_instance;
	}
	public function insertObj($table, $obj)
	{
		$sql = 'insert into ' . $table . ' ( ';
		$sqlValues = ' values ( ';
		$sqlParameter = $this->getSqlParameter ( $obj );
		$sql = $sql . $sqlParameter ['sqlColumns'] . ' ) ' . $sqlValues . $sqlParameter ['sqlValues'] . ' ) ';
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			return $STH->execute ( $sqlParameter ['valueArray'] );
		} 
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public function selectObj($table, $obj, $col = null)
	{
		$sql = 'select ';
		$sqlValues = ' from ' . $table . ' where   ';
		$first = true;
		$sqlCondition = '';
		if ($col == null)
		{
			$sql = $sql . ' * ';
		} else
		{
			$sql = $sql . implode ( ',', $col );
		}
		$objVars = get_object_vars ( $obj );
		$valueArray = array ();
		foreach ( $objVars as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$first = false;
					$sqlValues = $sqlValues . $k . ' = :' . $k;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . ' = :' . $k;
			
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			$STH->setFetchMode ( PDO::FETCH_OBJ );
			$STH->execute ( $valueArray );
			return $STH->fetchAll ();
		} 
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public function deleteObj($table, $obj)
	{
		$sql = 'delete ';
		$sqlValues = ' from ' . $table . ' where   ';
		$first = true;
		$sqlCondition = '';
		$objVars = get_object_vars ( $obj );
		$valueArray = array ();
		foreach ( $objVars as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$first = false;
					$sqlValues = $sqlValues . $k . ' = :' . $k;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . ' = :' . $k;
			
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			return $STH->execute ( $valueArray );
		
		} 
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public function updateObj($table, $obj, $objCol)
	{
		$sql = 'update  ' . $table . ' set  ';
		$sqlValues = ' where ';
		$objVars = get_object_vars ( $obj );
		$first = true;
		$valueArray = array ();
		foreach ( $objVars as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [] = $v;				
				if ($first)
				{
					$sql = $sql . $k . '= ? ';
					$first=false;
					continue;
				}
				$sql = $sql . ' , ' . $k . '= ? ';
			}
		}
		$objVars = get_object_vars ( $objCol );
		$first = true;
		foreach ( $objVars as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [] = $v;
				if ($first)
				{
					$sqlValues = $sqlValues . $k . '= ? ';
					$first=false;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . '= ? ';
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			$STH->execute ( $valueArray );
		}
		 catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public  function insertArr($table,$insertArray)
	{
		$sql = 'insert into ' . $table . ' ( ';
		$sqlValues = ' values ( ';
		$valueArray=array();
		$first = true;
		$sqlColumns='';
		$sqlValues=' values(';
		foreach ( $insertArray as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$sqlColumns = $sqlColumns . $k;
					$first = false;
					$sqlValues = $sqlValues . ':' . $k;
					continue;
				}
				$sqlColumns = $sqlColumns . ',' . $k;
				$sqlValues = $sqlValues . ' , :' . $k;
			}
		}
		$sql = $sql . $sqlColumns . ' ) ' . $sqlValues . ' ) ';
		try
		{
			$STH = $this::$_DBH->prepare ( $sql );
			$result=$STH->execute ( $valueArray );
			return $result;
		}
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public  function selectArr($table,$selectArray,$selectCol=null)
	{

		$sql = 'select ';
		$sqlValues = ' from ' . $table . ' where   ';
		$first = true;
		$sqlCondition = '';
		if ($selectCol == null)
		{
			$sql = $sql . ' * ';
		} 
		else
		{
			$sql = $sql . implode ( ',', $selectCol );
		}
		foreach ( $selectArray as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$first = false;
					$sqlValues = $sqlValues . $k . ' = :' . $k;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . ' = :' . $k;
					
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			$STH->setFetchMode ( PDO::FETCH_ASSOC );
			$STH->execute ( $valueArray );
			return $STH->fetchAll ();
		} 
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public  function deleteArr($table,$deleteArray)
	{
		$sql = 'delete ';
		$sqlValues = ' from ' . $table . ' where   ';
		$first = true;
		$sqlCondition = '';
		$valueArray = array ();
		foreach ( $deleteArray as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$first = false;
					$sqlValues = $sqlValues . $k . ' = :' . $k;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . ' = :' . $k;
					
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			return $STH->execute ( $valueArray );
		
		}
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	public  function updateArr($table,$updateArray,$updateArrayCol)
	{
		$sql = 'update  ' . $table . ' set  ';
		$sqlValues = ' where ';
		$first = true;
		$valueArray = array ();
		foreach ( $updateArray as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [] = $v;				
				if ($first)
				{
					$sql = $sql . $k . '= ? ';
					$first=false;
					continue;
				}
				$sql = $sql . ' , ' . $k . '= ? ';
			}
		}
		$first = true;
		foreach ( $updateArrayCol as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [] = $v;
				if ($first)
				{
					$sqlValues = $sqlValues . $k . '= ? ';
					$first=false;
					continue;
				}
				$sqlValues = $sqlValues . ' and ' . $k . '= ? ';
			}
		}
		$sql = $sql . $sqlValues;
		try
		{
			$STH = self::$_DBH->prepare ( $sql );
			return $STH->execute ( $valueArray );
		}
		 catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	
	}
	public  function  getLastInsertId()
	{
		return $this::$_DBH->lastInsertId();
	}
	public function changeDB($dbsoure = 'default')
	{
		try
		{
			$DBAddress = DBConfig::$DBConfigArray [$dbsoure] ['DBAddress'];
			$user = DBConfig::$DBConfigArray [$dbsoure] ['user'];
			$password = DBConfig::$DBConfigArray [$dbsoure] ['password'];
			self::$dbh = new PDO ( $DBAddress, $user, $password, array (
					PDO::ATTR_PERSISTENT => true 
			) );
			return true;
		} catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			return false;
		}
	}
	public function excuteSql($sql)
	{
		try
		{
			$STH = self::$_DBH->query ( $sql );
			return  $STH->fetchAll();
		}
		catch ( PDOException $e )
		{
			$this->errCode = 1;
			$this->errMsg = $e->getMessage ();
			die ();
		}
	}
	private function getSqlParameter($obj)
	{
		$result = array ();
		$objVars = get_object_vars ( $obj );
		$valueArray = array ();
		$sqlColumns = '';
		$sqlValues = '';
		$first = true;
		foreach ( $objVars as $k => $v )
		{
			if ($v != null)
			{
				$valueArray [$k] = $v;
				if ($first)
				{
					$sqlColumns = $sqlColumns . $k;
					$first = false;
					$sqlValues = $sqlValues . ':' . $k;
					continue;
				}
				$sqlColumns = $sqlColumns . ',' . $k;
				$sqlValues = $sqlValues . ' , :' . $k;
			}
		}
		$result ['sqlColumns'] = $sqlColumns;
		$result ['sqlValues'] = $sqlValues;
		$result ['valueArray'] = $valueArray;
		return $result;
	}

}
// $db = PdoDB::getInstance ();
// $stat = new Statistics ();
// $stat->what = 1;
// $stat->whatDevice = 2;
// $stat2 = new Statistics ();
// $stat2->what = 2;
// $stat2->whatDevice = 2;
// $stat3=array('what'=>2,'whatDevice'=>2);
// $stat4=array('what'=>1,'whatDevice'=>2);
// $sql='SELECT * FROM statistics.stat201308';
// print_r ( $db->excuteSql($sql));
