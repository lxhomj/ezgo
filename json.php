<?php
/* 判断referer来源是否合法 */
/*
$referer = isset( $_SERVER['HTTP_REFERER'] ) ? $_SERVER['HTTP_REFERER'] : '';
if ( !preg_match("/^https?:\/\/([0-9a-z]+\.){1,2}(qq\.com|tencent\.com|paipai\.com|tenpay\.com|soso\.com)(\/.*)?$/i", $referer) )
{
    exit(0);
}
*/
 
// php 运行环境配置
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
//error_reporting(E_ALL);
//ob_start();
//set_magic_quotes_runtime(0);
// 保留: 某些函数中有使用 $magic_quotes_gpc
$magic_quotes_gpc = get_magic_quotes_gpc();
//date_default_timezone_set('Asia/Shanghai');

$lib_root = "/data/web/game2.weedo.hk/weedolib/";
define('LOG_ROOT', "/data/web/game2.weedo.hk/log/");
require_once($lib_root . '/lib/Config.php');
require_once($lib_root . 'etc/game.inc.php');
require_once(WEEDOLIB_PATH.'lib/Logger.php');

// 获得 module 名
if (!empty($_REQUEST['mod']))
{
    $mod_name = preg_replace("/[^a-z0-9A-Z]/", '',trim($_REQUEST['mod']));
}
else
{
    $mod_name = 'main';
}

$mod_file = WEEDOLIB_PATH."module/".$mod_name.'.php';

if ( !file_exists($mod_file) )
{
    // 记录访问的 url
    $queryString = $_SERVER['REQUEST_URI'] . '?' . $_SERVER['QUERY_STRING'];
    $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
    //Logger::err('CRI-UNEXPECTED-VISITOR:' . $queryString . '-refer:' . $referer);
    exit($mod_name.' is wrong-mod-name');
}

// 获得 act 名
if (!empty($_REQUEST['act']))
{
    $act_name = preg_replace("/[^a-z0-9A-Z]/", '', trim($_REQUEST['act']));
}
else
{
    $act_name = 'page';
}

$func_name = $mod_name . '_' . $act_name;


require_once $mod_file;

if (!function_exists($func_name))
{
    $func_name = $mod_name . '_page';
    if(!function_exists($func_name))
    {
        $queryString = $_SERVER['REQUEST_URI'] . '?' . $_SERVER['QUERY_STRING'];
        $referer = empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
        //Logger::err('CRI-UNEXPECTED-VISITOR:' . $queryString . '-refer:' . $referer);
        exit('wrong-function-name');
    }
}

echo json_encode($func_name());

?>