<?php
/**
 * Common.php
 *
 * @date          	2012-11-8	
 * @Copyright     	(c)2012 Weedo Inc. All Rights Reserved
 * @Author        	shanks <shanks@weedo.hk>
 * @Version       	1.0.0
 */
 
function httpReq($url, $params)
{
	$strParams = http_build_query($params);
	var_dump($url . "?" . $strParams);
	//var_dump($strParams);
	$data = NetUtil::cURLHTTPPost($url, $strParams);
	
  
	
	return $data;  
}


function checkTestResult($testName, $result)
{
	echo "testCase($testName):";
	if(isset($result->ret) && $result->ret == 0)
	{
		echo "test passed\n";
	}
	else 
	{
		echo sprintf("test failed[errCode=%s][errMsg=%s]\n", $result->ret, $result->errmsg);
	}
}
?>